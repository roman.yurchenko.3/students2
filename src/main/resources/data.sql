insert into courses (id, name, description, required_grade)
values (1, 'Math', 'Algebra and Geometry', 9);
insert into courses (id, name, description, required_grade)
values (2, 'Java', 'Java core', 6);
insert into courses (id, name, description, required_grade)
values (3, 'Frontend', 'JS, HTML and CSS', 4);

insert into students (id, name, age, course, grade)
values (1, 'Jake', 22, 1, 10);
insert into students (id, name, age, course, grade)
values (2, 'Alex', 20, 2, 9);
insert into students (id, name, age, course, grade)
values (3, 'John', 21, 2, 8);
insert into students (id, name, age, course, grade)
values (4, 'Oleg', 19, 3, 5);
