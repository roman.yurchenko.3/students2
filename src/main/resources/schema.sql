create table students
(
    id     int primary key,
    name   varchar not null,
    age    int     not null,
    course int     not null,
    grade  int     not null
);

create table courses
(
    id             int primary key,
    name           varchar not null,
    description    varchar not null,
    required_grade int     not null
);