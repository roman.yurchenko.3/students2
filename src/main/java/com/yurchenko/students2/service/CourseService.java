package com.yurchenko.students2.service;

import com.yurchenko.students2.model.Course;
import com.yurchenko.students2.repository.CourseRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class CourseService {
    private final CourseRepository repository;

    public CourseService(CourseRepository repository) {
        this.repository = repository;
    }

    public Course create(long id, String name, String description, int requiredGrade) {
        return new Course(id, name, description, requiredGrade);
    }

    public void save(Course course) {
        repository.insert(course);
    }

    public Course createAndSave(long id, String name, String description, int requiredGrade) {
        Course course = create(id, name, description, requiredGrade);
        repository.insert(course);
        return course;
    }

    public Course findByName(String name) {
        return repository.findByName(name);
    }

    public List<Course> findAll() {
        return repository.findAll();
    }

    public Course findById(long id) {
        return repository.findById(id).orElseThrow(() -> new NoSuchElementException("Course not found by id=" + id));
    }

    public void deleteCourse(long id) {
        repository.deleteCourse(id);
    }

    public void updateCourse(Course course) {
        repository.updateCourse(course);
    }

}
