package com.yurchenko.students2.service;

import com.yurchenko.students2.model.Course;
import com.yurchenko.students2.model.Student;
import com.yurchenko.students2.repository.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.List;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class StudentService {
    private final StudentRepository repository;

    public StudentService(StudentRepository repository) {
        this.repository = repository;
    }

    public void save(Student student) {
        repository.insert(student);
    }

    public Student create(long id, String name, int age, Course course, int grade) {
        return new Student(id, name, age, course, grade);
    }

    public Student createAndSave(long id, String name, int age, Course course, int grade) {
        Student student = create(id, name, age, course, grade);
        save(student);
        return student;
    }

    public Student findById(long id) {
        return repository.findById(id).orElseThrow(() -> new NoSuchElementException("Student not found by id=" + id));
    }

    public List<Student> findAll() {
        return repository.findAll();
    }

    public void deleteStudent(long id) {
        repository.deleteStudent(id);
    }

    public void updateStudent(Student student) {
        repository.updateStudent(student);
    }

}
