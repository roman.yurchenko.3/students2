package com.yurchenko.students2.repository;

import com.yurchenko.students2.model.Course;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Optional;

@Mapper
public interface CourseRepository {

    void insert(Course course);

    Course findByName(String name);

    Optional<Course> findById(long id);

    List<Course> findAll();

    void deleteCourse(long id);

    void updateCourse(Course course);

}
