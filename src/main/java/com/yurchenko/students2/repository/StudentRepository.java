package com.yurchenko.students2.repository;

import com.yurchenko.students2.model.Student;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Optional;

@Mapper
public interface StudentRepository {

    void insert(Student student);

    Optional<Student> findById(long id);

    List<Student> findAll();

    void deleteStudent(long id);

    void updateStudent(Student student);
}
