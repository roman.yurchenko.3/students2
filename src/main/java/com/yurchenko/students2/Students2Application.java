package com.yurchenko.students2;

import com.yurchenko.students2.service.CourseService;
import com.yurchenko.students2.service.StudentService;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Students2Application {

    public static void main(String[] args) {
        SpringApplication.run(Students2Application.class, args);
    }


    @Bean
    ApplicationRunner sampleCommandLineRunner(StudentService studentService, CourseService courseService) {
        return args -> {
            //Create
            System.out.println("Create section:");
            var course = courseService.createAndSave(5,"Python", "Fullstack", 7);
            var student = studentService.createAndSave(12, "Logan", 21, course, 7);
            System.out.println(course);
            System.out.println(student);
            System.out.println();


            //Read
            System.out.println("Read section:");
            System.out.println(studentService.findById(3));
            System.out.println(studentService.findAll());
            System.out.println(courseService.findById(5));
            System.out.println(courseService.findByName("Frontend"));
            System.out.println(courseService.findAll());
            System.out.println();

            //Update
            System.out.println("Update section:");
            student.setName("William");
            student.setAge(24);
            student.setGrade(10);
            student.setCourse(courseService.findById(3));
            studentService.updateStudent(student);
            System.out.println(studentService.findById(student.getId()));

            course.setName(course.getName() + "!");
            course.setDescription(course.getDescription() + "!!");
            course.setRequiredGrade(4);
            courseService.updateCourse(course);
            System.out.println(courseService.findById(course.getId()));
            System.out.println();

            //Delete
            System.out.println("Delete section:");
            studentService.deleteStudent(1);
            studentService.deleteStudent(2);
            courseService.deleteCourse(1);
            System.out.println(studentService.findAll());
            System.out.println(courseService.findAll());

        };
    }
}
