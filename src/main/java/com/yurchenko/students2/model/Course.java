package com.yurchenko.students2.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Course {
    private long id;
    private String name;
    private String description;
    private int requiredGrade;
}
